/* eslint-disable react/no-unescaped-entities */
import React, { useState } from 'react';
import Sidebar from '../../components/Sidebar/Sidebar';
import {
  BookNow,
  BookContent,
  BookTitle,
  NextButton,
  BookTab,
  Flex,
  FlexBetween,
  CollapseHidden,
  CollapseArrow,
  Price,
  Checkbox,
  CheckboxChecked,
} from './ExtraServicesStyled';
import arrow from '../../assets/icons/arrow.svg';
import check from '../../assets/icons/check.svg';
import owen from '../../assets/icons/owen.svg';
import frige from '../../assets/icons/frige.svg';
import moveIn from '../../assets/icons/moveIn.svg';
import deep from '../../assets/icons/deep.svg';
import { Link } from 'react-router-dom';
import { Collapse } from 'reactstrap';

const ExtraServices = () => {
  return (
    <BookNow>
      <Sidebar />
      <BookContent>
        <BookTitle>Do you need some extra services?</BookTitle>

        <BookTab collapsed={true}>
          <FlexBetween>
            <Flex>
              <img src={frige} alt='Area' />
              <p style={{ marginLeft: 20 }}>{'Inside refrigerator'}</p>
              <CollapseArrow
                collapsed={true}
                src={arrow}
                alt='arrow'
                className='ml-3'
              />
            </Flex>
            <Flex>
              {/* <Checkbox></Checkbox> */}
              <CheckboxChecked>
                <img src={check} alt='Area' />
              </CheckboxChecked>
            </Flex>
          </FlexBetween>

          <CollapseHidden>
            <p>
              Peak Demand can be defined as the overall power requested at the
              summit of a given time.{' '}
            </p>
            <p>
              Bring to the table win-win survival strategies to ensure proactive
              domination. At the end of the day, going forward, a new normal
              that has evolved from generation X is on the runway.
            </p>
            <p>
              At the end of the day, going forward, a new normal that has
              evolved from generation X.
            </p>
            <Price>$50</Price>
          </CollapseHidden>
        </BookTab>

        <TabCollapse icon={frige} title={'Inside refrigerator'} />
        <TabCollapse icon={owen} title={'Inside Owen'} />
        <TabCollapse icon={moveIn} title={'Move In - Move Out'} />
        <TabCollapse icon={deep} title={'Deep Cleaning'} />

        <Link to='/AdditionalQuestions'>
          <NextButton>next</NextButton>
        </Link>
      </BookContent>
    </BookNow>
  );
};

const TabCollapse = (props) => {
  return (
    <BookTab>
      <FlexBetween>
        <Flex>
          <img src={props.icon} alt='Area' />
          <p style={{ marginLeft: 20 }}>{props.title}</p>
          <CollapseArrow
            collapsed={false}
            src={arrow}
            alt='arrow'
            className='ml-3'
          />
        </Flex>
        <Flex>
          <Checkbox></Checkbox>
        </Flex>
      </FlexBetween>
    </BookTab>
  );
};

export default ExtraServices;
